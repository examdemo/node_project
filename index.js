var express = require('express');
var app = express();
var mysql = require('mysql')
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database:'node'
})

//create table movie(movie_id int primary key auto_increment,movie_title varchar(50), 
//movie_release_date varchar(50), movie_time varchar(50),director_name varchar(50));

conn.connect(function (err) {
    if (err) throw err;
    console.log("connection successfull");
})

app.post('/insert', function (req, res) {
    var movie_title = req.body.movie_title;
    var movie_release_date = req.body.movie_release_date;
    var movie_time = req.body.movie_time;
    var director_name = req.body.director_name;
 
    var sql = `insert into movie(movie_title,movie_release_date,movie_time,director_name) values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`;
    conn.query(sql, function (err, result) {
        if (err) throw err; 
        res.send('<h1> movie inserted....!</h1>')
    })
})

app.get('/show', function (req, res) {

    var sql = `select * from movie`;
    conn.query(sql, function (err, results) {
        if (err) throw err;
        res.send({ movies: results });
    })
})


app.get('/delete/:id', function (req, res) {
    var id = req.params.id;
    var sql = `delete from movie where movie_id=${id}`;
    conn.query(sql, function (err, result) {
        if (err) throw err;
        res.redirect('/show');
    });
}) 

var server = app.listen(4000, function () {
    console.log("app running on port 4000....")
});